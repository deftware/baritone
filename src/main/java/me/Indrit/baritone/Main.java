package me.Indrit.baritone;

import com.google.gson.*;
import me.deftware.client.framework.main.Bootstrap;
import me.deftware.client.framework.main.EMCMod;
import me.deftware.client.framework.utils.ChatColor;
import me.deftware.client.framework.utils.OSUtils;
import me.deftware.client.framework.wrappers.IChat;
import me.deftware.client.framework.wrappers.IMinecraft;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main extends EMCMod {

    private Logger LOGGER = LogManager.getLogger("Baritone/Fabritone Installer");
    private File EMC_DIR;
    private final String DATA_URL = "https://gitlab.com/EMC-Framework/maven/raw/master/marketplace/plugins/Baritone/data.json";
    private final String BARITONE_TWEAKER_LIB = OSUtils.getMCDir() + "libraries" + File.separator + "cabaletta" + File.separator + "baritone" + File.separator + IMinecraft.getMinecraftVersion() + File.separator;
    private JsonObject modData;

    @Override
    public void initialize() {
        try {
            EMC_DIR = new File(OSUtils.getMCDir() + "libraries" + File.separator + "me" + File.separator + "deftware" + File.separator + "EMC-F" + File.separator + getEMCVersion() + File.separator);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // Install Baritone/Fabritone upon initialization, and verify/update existing installations
        new Thread(() -> {
            try {
                modData = new Gson().fromJson(get(DATA_URL), JsonObject.class).get(IMinecraft.getMinecraftVersion()).getAsJsonObject();

                if (modData.get("available").getAsBoolean()) {
                    // Install Baritone and apply tweaker
                    if (modData.get("type").getAsString().equals("tweakClass")) {
                        // Auto update is not available on <1.14 currently, because of how Windows filesystems work
                        File baritone = new File(BARITONE_TWEAKER_LIB + "baritone-" + IMinecraft.getMinecraftVersion() + ".jar");
                        if (!baritone.exists()) {
                            install(modData.get("url").getAsString(), baritone);
                            updateAristoisJson(true);
                        }
                    } else if (modData.get("type").getAsString().equals("fabric")) {
                        // Install Fabritone for mc 1.14+
                        File fabritone = new File(EMC_DIR.getAbsolutePath() + "baritone-" + IMinecraft.getMinecraftVersion() + ".jar");
                        if (!fabritone.exists()) {
                            install(modData.get("url").getAsString(), fabritone);
                        } else if (fabritone.exists() && !this.getSettings().getString("baritone_version", "").equals(modData.get("version").getAsString())) {
                            // Update fabritone using .update files
                            LOGGER.info("Updating Fabritone");
                            install(modData.get("url").getAsString(), new File(EMC_DIR.getAbsolutePath() + "baritone-" + IMinecraft.getMinecraftVersion() + "_update.jar"));
                        } else {
                            IChat.sendClientMessage(ChatColor.LIGHT_PURPLE + "USE \"@\" to use baritone commands");
                        }
                    }
                } else {
                    LOGGER.error("Baritone not available for " + IMinecraft.getMinecraftVersion());
                    Bootstrap.callMethod("EMC-Marketplace", "setStatus(Baritone is not available for " + IMinecraft.getMinecraftVersion() + ")", "Baritone Installer", null);
                }
                message(ChatColor.GRAY + "USE \"@\" to use baritone commands");
                if (IMinecraft.getMinecraftVersion().equals("1.13.2")) {
                    message(ChatColor.GRAY + "Source: https://gitlab.com/emc-mods-indrit/baritone_api/tree/master");
                } else {
                    message(ChatColor.GRAY + "Source: https://gitlab.com/deftware/fabritone");
                }
                message(ChatColor.GRAY + "OG Source: https://github.com/cabaletta/baritone");
            } catch (Exception ex) {
                IChat.sendClientMessage("Error: Failed to install Baritone, see latest.log");
                LOGGER.error("Failed to install Baritone:");
                LOGGER.error("Minecraft version: " + IMinecraft.getMinecraftVersion());
                Bootstrap.callMethod("EMC-Marketplace", "setStatus(Could not install Baritone, see latest.log)", "Baritone Installer", null);
                ex.printStackTrace();
            }
        }).start();
    }

    private void install(String url, File path) throws Exception {
        Bootstrap.callMethod("EMC-Marketplace", "setStatus(Installing Baritone, do not close Minecraft...)", "Baritone Installer", null);
        download(url, path);
        Bootstrap.callMethod("EMC-Marketplace", "setStatus(Installed Baritone, please restart Minecraft and the Minecraft launcher)", "Baritone Installer", null);
    }

    private void message(String msg){
        IChat.sendClientMessage(msg, ChatColor.DARK_PURPLE + "[" + ChatColor.LIGHT_PURPLE + "Baritone" + ChatColor.DARK_PURPLE + "]");
    }

    private void updateAristoisJson(boolean add) throws Exception {
        File jsonFile = new File(OSUtils.getRunningFolder() + OSUtils.getVersion() + ".json");
        if (!jsonFile.exists()) {
            System.err.println("Could not find json file!");
        } else {
            JsonObject jsonObject = new Gson().fromJson(String.join("", Files.readAllLines(Paths.get(jsonFile.getAbsolutePath()), StandardCharsets.UTF_8)), JsonObject.class);
            JsonArray game = jsonObject.get("arguments").getAsJsonObject().get("game").getAsJsonArray();
            JsonArray libraries = jsonObject.get("libraries").getAsJsonArray();

            String classTweaker = "baritone.launch.BaritoneTweaker";

            if (add) {
                libraries.getAsJsonArray().add(new JsonParser().parse("{\"name\": \"com.github.ImpactDevelopment:SimpleTweaker:1.2\",\"url\": \"https://impactdevelopment.github.io/maven/\"}").getAsJsonObject());
                libraries.getAsJsonArray().add(new JsonParser().parse("{\"name\": \"org.ow2.asm:asm-all:5.2\",\"url\": \"https://repo1.maven.org/maven2/\"}").getAsJsonObject());
                libraries.getAsJsonArray().add(new JsonParser().parse("{\"name\": \"cabaletta:baritone:"+IMinecraft.getMinecraftVersion()+"\"}").getAsJsonObject());

                game.add("--tweakClass");
                game.add(classTweaker);
            } else {
                for (int i = 0; i < libraries.size() - 1; i++) {
                    if (libraries.get(i).getAsJsonObject().get("name").getAsString().startsWith("com.github.ImpactDevelopment")){
                        libraries.get(i).getAsJsonObject().remove("name");
                        libraries.get(i).getAsJsonObject().remove("url");
                        libraries.remove(i);
                    }
                    if (libraries.get(i).getAsJsonObject().get("name").getAsString().equals("org.ow2.asm:asm-all:5.2")){
                        libraries.get(i).getAsJsonObject().remove("name");
                        libraries.get(i).getAsJsonObject().remove("url");
                        libraries.remove(i);
                    }
                    if (libraries.get(i).getAsJsonObject().get("name").getAsString().startsWith("cabaletta:baritone")){
                        libraries.get(i).getAsJsonObject().remove("name");
                        libraries.remove(i);
                    }
                }
                for(int i = 0; i < game.size() - 1; i++) {
                    if (game.get(i).getAsString().equals("--tweakClass") && game.get(i+1).getAsString().equals(classTweaker)) {
                        game.remove(i);
                        game.remove(i);
                        break;
                    }
                }
            }

            // Save
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            JsonParser jp = new JsonParser();
            JsonElement je = jp.parse(jsonObject.toString());
            String jsonContent = gson.toJson(je);
            PrintWriter writer = new PrintWriter(jsonFile.getAbsolutePath(), "UTF-8");
            writer.println(jsonContent);
            writer.close();
        }
    }

    private  String getEMCVersion() throws Exception {
        File jsonFile = new File(OSUtils.getRunningFolder() + OSUtils.getVersion() + ".json");
        String version = null;
        if (!jsonFile.exists()) {
            throw new Exception("Could not find EMC json file!");
        } else {
            JsonObject jsonObject = new Gson().fromJson(String.join("", Files.readAllLines(Paths.get(jsonFile.getAbsolutePath()), StandardCharsets.UTF_8)), JsonObject.class);
            for (JsonElement jsonElement : jsonObject.get("libraries").getAsJsonArray()) {
                JsonObject entry = jsonElement.getAsJsonObject();
                if (entry.get("name").getAsString().contains("me.deftware:EMC-F")) {
                    version = entry.get("name").getAsString().split(":")[2];
                }
            }
        }
        return version;
    }

    @Override
    public void callMethod(String method, String caller, Object object) {
        if (method.equalsIgnoreCase("uninstall()")) {
            try {
                LOGGER.info("Uninstalling Baritone...");
                Bootstrap.callMethod("EMC-Marketplace", "setStatus(Uninstalling Baritone, do not close Minecraft...)", "Baritone Un-Installer", null);
                if (modData.get("type").getAsString().equals("tweakClass")) {
                    updateAristoisJson(false);
                } else if (modData.get("type").getAsString().equals("fabric")) {
                    FileUtils.writeStringToFile(new File(EMC_DIR.getAbsolutePath() + "baritone-" + IMinecraft.getMinecraftVersion() + ".jar.delete"), "Delete mod", "UTF-8");
                }
                this.getSettings().saveString("baritone_version", "");
                this.getSettings().saveConfig();
                Bootstrap.callMethod("EMC-Marketplace", "setStatus(Uninstalled Baritone, please restart Minecraft and the Minecraft launcher)", "Baritone Un-Installer", null);
                LOGGER.info("Uninstalled Baritone");
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void download(String uri, File fileName) throws Exception {
        URL url = new URL(uri);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        connection.setRequestMethod("GET");
        FileOutputStream out = new FileOutputStream(fileName.getAbsolutePath());
        InputStream in = connection.getInputStream();
        int read;
        byte[] buffer = new byte[4096];
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
        in.close();
        out.close();
    }

    private String get(String url) throws Exception {
        URL url1 = new URL(url);
        Object connection = (url.startsWith("https://") ? (HttpsURLConnection) url1.openConnection()
                : (HttpURLConnection) url1.openConnection());
        ((URLConnection) connection).setConnectTimeout(10 * 1000);
        ((URLConnection) connection).setRequestProperty("User-Agent", "EMC Installer");
        ((HttpURLConnection) connection).setRequestMethod("GET");
        BufferedReader in = new BufferedReader(new InputStreamReader(((URLConnection) connection).getInputStream()));
        StringBuilder result = new StringBuilder();
        String text;
        while ((text = in.readLine()) != null) {
            result.append(text);
        }
        in.close();
        return result.toString();
    }

}
